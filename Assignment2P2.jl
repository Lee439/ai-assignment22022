### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 269189fe-1b50-45ec-b1f5-f7e01e7c6aa1
mutable struct s_Function
	 name::String
     input::Any
	 output::Any

end

# ╔═╡ afea83a0-cbaf-11ec-0407-81b590b9ba73
mutable struct Var
	name::String
    value::Union{Nothing,s_Function}
    forbidden_values::Vector{s_Function}
	domain_restriction_count::Int64
	time::Float64
	latency::Float64
	throughput::Float64
	successRate::Float64
end

# ╔═╡ 17ddb6cf-135c-4913-ae3c-c72f3b1fdd79
struct sCSP
	vars::Vector{Var}
	constraints::Vector{Tuple{Var,Var}}
end


# ╔═╡ 7f95e32d-ee1e-4309-ae9d-90f0e4ad4aca
f1 =  s_Function("f1","int","double")

# ╔═╡ 1ea8cd92-68aa-482f-b3d4-255b347ef863
f2 =  s_Function("f2","int","int")

# ╔═╡ d0c33cc8-800a-48c7-bf13-897be3c5a6b9
f3 =  s_Function("f3","string","bool")

# ╔═╡ ceef8d1d-01aa-40c5-977c-dae871c2f3de
f4 =  s_Function("f4","int","bool")

# ╔═╡ da02b96d-d8f3-4e6c-93a0-02b38ea64532
all_functons = [f1,f2,f3,f4]

# ╔═╡ 2c53adc2-4cf8-48c7-8ee9-b8fe9730ad4f
rand(setdiff(Set(all_functons),Set([f1,f2])))

# ╔═╡ 10599619-5533-42d1-9427-04674904af84
function check(all_function ,SeverlessFunction::s_Function)

	for func in all_function
      if func.input == SeverlessFunction.input

	  end
	end
 
  return SeverlessFunction
end

# ╔═╡ f1a3eda5-d79f-4541-9df7-f3e355e62371
function solve(pb::sCSP, all_assignments)
		for current_variable in pb.vars
			if current_variable.domain_restriction_count == 4
				return []
			else
				next_val = rand(setdiff(Set(all_functons),
Set(current_variable.forbidden_values)))

			
				for cur_constraint in pb.constraints
					if !((cur_constraint[1] == current_variable) || (cur_constraint[2] ==
							current_variable))
						continue
					  else
					if cur_constraint[1] == current_variable
								push!(cur_constraint[2].forbidden_values, next_val)
								cur_constraint[2].domain_restriction_count += 1
						
                    else
                      push!(cur_constraint[1].forbidden_values, next_val)
                       cur_constraint[1].domain_restriction_count += 1
                    

		          end
              end
          end
       
      push!(all_assignments, current_variable.name => next_val)
      end
   end
return all_assignments
end



# ╔═╡ 7bfea349-c26c-465d-917d-3b20147ddd05
a1 = Var("action1",nothing, [], 0,0.5,0.2,0.5,0.5)

# ╔═╡ 8f576ede-0ef3-49d8-928e-1705b1ea48ac
 a2 = Var("action2", nothing, [], 0,0.2,0.2,0.5,0.5)

# ╔═╡ 8282a813-88d9-4e81-aa97-968bd6a7c30e
a3 = Var("action3", nothing, [], 0,0.4,0.2,0.5,0.5)

# ╔═╡ d902fe5a-dc42-468b-8a07-8c82d10cbab5
 a4 = Var("action4", nothing, [], 0,0.2,0.2,0.5,0.5)

# ╔═╡ 7b66229e-71a3-4f14-8231-5b4fe60c1485
 a5 = Var("action5", nothing, [], 0,0.9,0.2,0.5,0.5)

# ╔═╡ b03c1d85-19ff-4099-8c71-15ab9086ec99
problem = sCSP([a1, a2, a3, a4,a5], [(a1,a3), (a1,a4), (a2,a4),
(a3,a5),(a1,a4),(a1,a5)])

# ╔═╡ b910157e-28c3-4de0-8c0d-4c57234a5fdd
solve(problem, [])


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═269189fe-1b50-45ec-b1f5-f7e01e7c6aa1
# ╠═afea83a0-cbaf-11ec-0407-81b590b9ba73
# ╠═17ddb6cf-135c-4913-ae3c-c72f3b1fdd79
# ╠═7f95e32d-ee1e-4309-ae9d-90f0e4ad4aca
# ╠═1ea8cd92-68aa-482f-b3d4-255b347ef863
# ╠═d0c33cc8-800a-48c7-bf13-897be3c5a6b9
# ╠═ceef8d1d-01aa-40c5-977c-dae871c2f3de
# ╠═da02b96d-d8f3-4e6c-93a0-02b38ea64532
# ╠═2c53adc2-4cf8-48c7-8ee9-b8fe9730ad4f
# ╠═10599619-5533-42d1-9427-04674904af84
# ╠═f1a3eda5-d79f-4541-9df7-f3e355e62371
# ╠═7bfea349-c26c-465d-917d-3b20147ddd05
# ╠═8f576ede-0ef3-49d8-928e-1705b1ea48ac
# ╠═8282a813-88d9-4e81-aa97-968bd6a7c30e
# ╠═d902fe5a-dc42-468b-8a07-8c82d10cbab5
# ╠═7b66229e-71a3-4f14-8231-5b4fe60c1485
# ╠═b03c1d85-19ff-4099-8c71-15ab9086ec99
# ╠═b910157e-28c3-4de0-8c0d-4c57234a5fdd
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
