### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ c03d2072-cb26-11ec-3f33-73d3752169b5
using DataStructures

# ╔═╡ 7ede4994-5177-44e4-a9d2-3dd50a033199
md"## Creating States "

# ╔═╡ 9a3b4424-3e3b-4c53-81ca-cc10b125b76d
mutable struct CompanyX
	NumFlo::Int
	room_Number::Int
	hasAgent::Bool
	parcels::Int
	
function CompanyX(  NumFlo,room_Number, hasAgent, parcels )
		return new(NumFlo,room_Number, hasAgent, parcels)
	end
end

# ╔═╡ 21dac530-b6dc-4ef8-8ca1-109cc3a19d09
struct ction
name::String
cost::Int64
end


# ╔═╡ 519656a5-7bb6-4ba1-9def-a53176845af3
md"## Creating Actions"

# ╔═╡ 9ce8ce93-a6b7-4e84-a753-d68ad0cc9cc6
begin 
M_E=ction("me",2)
M_W=ction("mw",2)
M_U=ction("mu",1)
M_D=ction("md",1)
C_O=ction("co",5)

end 

# ╔═╡ 506768d2-3d52-4bbe-86ee-747abfb7144a
@enum Action ME MW CO MU MD

# ╔═╡ 85402b0f-6784-4c03-80dc-8752d956d26b
struct State
	Building::Array{CompanyX}
	
end

# ╔═╡ a6700627-0e33-4dcf-8970-195a44b52532
struct Node
	state::State
	parent::Union{Nothing,Node}
	action::Union{Nothing,Action}
end

# ╔═╡ a86905ff-c345-459c-9c4b-6490e416883b
md"## Transtion Model"

# ╔═╡ e2c68da8-69b8-46d3-8913-d3eb74cf69c1


function a_star(graph, start_node, target_node)
	
G_SCORE = 0  // Used to index g-score
F_SCORE = 1  // Used to index f-score
PREVIOUS = 2  // Used to index previous
        
    visited = {}  // Declare visited list as empty dictionary    
    unvisited = {}  // Declare unvisited list as empty dictionary

    // Add every node to the unvisited list
    FOREACH key IN graph
        unvisited[key] = [∞ , ∞ , Null] 
    NEXT key

    // Update values for start node in unvisited list
    h_score = heuristic(start_node)
    unvisited[start_node] = [0, h_score, NULL]  
    
    // Repeat until there are no nodes in the unvisited list
    finished = False
    WHILE finished == False
        IF LEN(unvisited) == 0 THEN  // No nodes left to evaluate
            finished = True 
        ELSE
	    // Get node with lowest f-score from open list
            current_node = get_minimum(unvisited) 
            IF current_node == target_node THEN
                finished = True
		// Copy data to visited list  
                visited[current_node] = unvisited[current_node]              
            ELSE
		// Examine neighbours
                FOREACH neighbour IN graph[current_node]
		    // Only check unvisited neighbours
                    IF neighbour NOT IN visited THEN
			// Calculate new g-score
                        new_g_score = unvisited[current_node][G_SCORE] + graph[current_node][neighbour]
                        // Check if new g-score is less						
                        IF new_g_score < unvisited[neighbour][G_SCORE] THEN
                            unvisited[neighbour][G_SCORE] = new_g_score
                            unvisited[neighbour][F_SCORE] = new_g_score + heuristic(neighbour)
                            unvisited[neighbour][PREVIOUS] = current_node
			ENDIF
		    ENDIF
		NEXT neighbour
                
		// Add current node to visited list
                visited[current_node] = unvisited[current_node]
		// Remove from unvisited list
                DEL(unvisited[current_node])
	    ENDIF
	ENDIF
    ENDWHILE
    
    // Return final visited list
    RETURN visited
	end 

# ╔═╡ f6133821-5b68-4230-8ed5-ae2e2932f627
function transModel(node::Node, RoomPF, FloorNum)
	state = node.state
	transitions = Dict{Action,Node}()

	for (i,j) in enumerate(state.Building)
		
		if(j.hasAgent)

				# move left
			if(j.room_Number > 1)
			   C_state = deepcopy(state)
			
				C_state.Building[i].hasAgent = false
				C_state.Building[i-1].hasAgent = true
				transitions[MW] = Node(C_state,node,MW)
			end

			# move up 
			if(j.NumFlo < FloorNum)
				C_state = deepcopy(state)
				C_state.Building[i].hasAgent = false
				
				C_state.Building[i+RoomPF].hasAgent = true
				transitions[MU] = Node(C_state,node,MU)
			end
			  #move right
			if(j.room_Number < RoomPF)
				C_state = deepcopy(state)
				C_state.Building[i].hasAgent = false
				C_state.Building[i+1].hasAgent = true
				transitions[ME] = Node(C_state,node,ME)
				
			end

		
            #move down
			if(j.NumFlo > 1)
				C_state = deepcopy(state)
				C_state.Building[i].hasAgent = false
				C_state.Building[i-RoomPF].hasAgent = true
				transitions[MD] = Node(C_state,node,MD)
			end
          

				if(j.parcels > 0)
				C_state = deepcopy(state)
				C_state.Building[i].parcels = C_state.Building[i].parcels - 1
				transitions[CO] = Node(C_state,node,CO)
			end
		end
	end

	return transitions

end

# ╔═╡ c3e4c33a-21d9-47e9-9137-d49fb933433a
md"## Finding optimal Path"

# ╔═╡ 3bf847df-5ae2-42ce-adb5-f25b8f4e6adf
function is_goal(state::State)
	for room in state.Building
		if(room.parcels > 0)
			return false
		end
	end
	return true
end

# ╔═╡ e6199170-c721-45fa-a44a-d0e3d1de3cb9
function get_path(node::Node)
	path = [node]
	 while !isnothing(node.parent)
		 node = node.parent
		 pushfirst!(path,node)
		 
	 end
	return path

end

# ╔═╡ 238aeaee-659d-4c94-a49a-4dec83098dd3
function solution(node::Node ,explored::Array)
	path = get_path(node)
    actions = []

	for node in path
		if !isnothing(node.action)
            push!(actions,node.action)
		end
	end

	cost = length(actions)
     return cost,"found ,$(length(actions)) stop solution in $(length(explored)) steps: $(join(actions," > "))"
end

# ╔═╡ 506081f3-e5d8-4ff0-9a02-e2e9008739e7
function failure(message::String)
   return -1,message
end

# ╔═╡ bf5fd4c5-c012-41d7-a126-52ca3a00e4b3
initial_state = State([
	CompanyX(1, 1,false,1),CompanyX(1, 2,false,1 ),
	CompanyX(2,3,true,1),CompanyX(2,4,false,1),
	CompanyX(3,2,false,1),CompanyX(3,2,false,1)
])

# ╔═╡ cd937408-ab46-49aa-9b18-f42813b32bef
transModel(Node(initial_state, nothing, nothing), 2, 2)

# ╔═╡ 5b5d09d6-0e0d-4090-9712-4963216d8dfc
actionCost = Dict(CO => 5, MD => 1,MU=>1,MW=>2,ME=>2)

# ╔═╡ 6d65443a-eda5-4e21-b811-3dc33505476b


# ╔═╡ b711765e-e7ce-407b-b2e8-d39603fd1052
md"## Calculating Total cost and Heuristic"

# ╔═╡ 480af36f-88dd-46d3-9015-d1e3847cc71a
function calculate_total_cost(node::Node)::Int
	totalCost = 0
	path = get_path(node)
   for node in path
		if !isnothing(node.action)
            totalCost+=actionCost[node.action]
		end
	end
	return totalCost
	 
end

# ╔═╡ 07e88f68-1a7c-468c-9177-5e4a0ff37a71
function heuristic(node::Node)
	totalParcels = 0
	for room in node.state.Building
		totalParcels += room.parcels
	    
	end
	return totalParcels * 3
end

# ╔═╡ e3a484fb-c41b-4bc5-8faf-2b448c73a033
function a_star_cost(node::Node)
   return calculate_total_cost(node) +heuristic(node)
  # return heuristic(node)
end

# ╔═╡ 61676284-0b66-4345-b029-7cda39ce16c1
md"## A star Algorithm"

# ╔═╡ 1194d631-e018-4b27-9699-a943103e595d


# ╔═╡ e07f50c2-d474-4940-9b4d-43f3baad2d28
function astar_search(start::State,FloorNum)


	RoomPF = size(initial_state.Building)[1] / FloorNum
	print(RoomPF)
	
	node = Node(start,nothing,nothing)
	
	if is_goal(node.state)
        return solution(node,"Parcels collected",[])
	end
	frontier = PriorityQueue{Node,Int}()
	enqueue!(frontier,node,a_star_cost(node))
	 
	explored = []
	count =  0 

	while true
		if isempty(frontier)
			return failure("Try again")
		end
        
		node = dequeue!(frontier)
		push!(explored, node.state)
		for(action,child) in transModel(node, Int(RoomPF), FloorNum)
			count += 1
			if !(child in keys(frontier) && !(child.state in explored))
				if is_goal(child.state)
					return solution(child,explored)
				end
				
				
				enqueue!(frontier,child,a_star_cost(child))
				
			end
			
		end#for
        if (count>10000000)
            return failure("timed out")
		end
	end
       
end


# ╔═╡ 42a2d6d5-1238-4247-8d83-d6a1367f8ecb
md"## Result"

# ╔═╡ d412d16b-3efe-4797-849c-5ee663cc1100
astar_search(initial_state, 3)

# ╔═╡ a7f485be-925c-484f-b251-8d4f2babdf72


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"

[compat]
DataStructures = "~0.18.11"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═c03d2072-cb26-11ec-3f33-73d3752169b5
# ╠═7ede4994-5177-44e4-a9d2-3dd50a033199
# ╠═9a3b4424-3e3b-4c53-81ca-cc10b125b76d
# ╠═21dac530-b6dc-4ef8-8ca1-109cc3a19d09
# ╠═519656a5-7bb6-4ba1-9def-a53176845af3
# ╠═9ce8ce93-a6b7-4e84-a753-d68ad0cc9cc6
# ╠═506768d2-3d52-4bbe-86ee-747abfb7144a
# ╠═85402b0f-6784-4c03-80dc-8752d956d26b
# ╠═a6700627-0e33-4dcf-8970-195a44b52532
# ╠═a86905ff-c345-459c-9c4b-6490e416883b
# ╠═e2c68da8-69b8-46d3-8913-d3eb74cf69c1
# ╠═f6133821-5b68-4230-8ed5-ae2e2932f627
# ╠═cd937408-ab46-49aa-9b18-f42813b32bef
# ╠═c3e4c33a-21d9-47e9-9137-d49fb933433a
# ╠═3bf847df-5ae2-42ce-adb5-f25b8f4e6adf
# ╠═e6199170-c721-45fa-a44a-d0e3d1de3cb9
# ╠═238aeaee-659d-4c94-a49a-4dec83098dd3
# ╠═506081f3-e5d8-4ff0-9a02-e2e9008739e7
# ╠═bf5fd4c5-c012-41d7-a126-52ca3a00e4b3
# ╠═5b5d09d6-0e0d-4090-9712-4963216d8dfc
# ╠═6d65443a-eda5-4e21-b811-3dc33505476b
# ╠═b711765e-e7ce-407b-b2e8-d39603fd1052
# ╠═480af36f-88dd-46d3-9015-d1e3847cc71a
# ╠═07e88f68-1a7c-468c-9177-5e4a0ff37a71
# ╠═e3a484fb-c41b-4bc5-8faf-2b448c73a033
# ╠═61676284-0b66-4345-b029-7cda39ce16c1
# ╠═1194d631-e018-4b27-9699-a943103e595d
# ╠═e07f50c2-d474-4940-9b4d-43f3baad2d28
# ╠═42a2d6d5-1238-4247-8d83-d6a1367f8ecb
# ╠═d412d16b-3efe-4797-849c-5ee663cc1100
# ╠═a7f485be-925c-484f-b251-8d4f2babdf72
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
